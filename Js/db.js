
// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries



import { getDatabase ,onValue, ref as refS, set, child, get, update, remove } 
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";


import { getStorage, ref, uploadBytesResumable, getDownloadURL }
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";
  // Your web app's Firebase configuration
const firebaseConfig = {

    apiKey: "AIzaSyA0LYU38Xx9RLdenzYeyiyo_YNTBVykE_A",
    authDomain: "administradorfruteria.firebaseapp.com",
    databaseURL: "https://administradorfruteria-default-rtdb.firebaseio.com",
    projectId: "administradorfruteria",
    storageBucket: "administradorfruteria.appspot.com",
    messagingSenderId: "724866144703",
    appId: "1:724866144703:web:f2cfd1fa6d2bf24914290a"
  };

  // Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage();

// variables para manejo de la imagen

const imageInput = document.getElementById('imageInput');
const uploadButton = document.getElementById('uploadButton');
const progressDiv = document.getElementById('progress');
const txtUrlInput = document.getElementById('txtUrl');

var codigo =0;
var  precio =0.0;
var cantidad =0;
var descripcion="";
var urlImag ="";

// listar productos
function Listarproductos() {
    const titulo= document.getElementById("titulo");
    const  section = document.getElementById('contenedorflexy')
    titulo.innerHTML= descripcion; 
    
    
  const dbRef = refS(db, 'Fruteria');
  const tabla = document.getElementById('tablaProductos');
 
  onValue(dbRef, (snapshot) => {
      snapshot.forEach((childSnapshot) => {     
          const childKey = childSnapshot.key;

          const data = childSnapshot.val();
         
          section.innerHTML+= "<div class ='card'> " +
                     "<img  id='urlImag' src=' "+ data.urlImag + "'  alt=''  width='200' height='200' > "
                     + "<h1 id='precio'  class='title'>" + data.precio+ ":" + data.cantidad +"</h1> "
                     + "<p  id='descripcion' class ='anta-regurar'>" + data.descripcion 
                     +  "</p> <button> Mas Informacion </button> </div>"  ;           


      });
  }, { onlyOnce: true });
}
Listarproductos("Frutas");